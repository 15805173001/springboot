package com.luo.thread;


import java.util.concurrent.CountDownLatch;

/**
 * 描述
 *
 * @author: 罗红兵
 * date:    2019/8/5 12:17
 */
public class TestCountDownLatch {

    public static void main(String[] args) throws InterruptedException {
        int thread = 3 ;
        long start = System.currentTimeMillis();
        final CountDownLatch countDown = new CountDownLatch(5);
        for (int i= 0 ;i<thread ; i++){
            int finalI = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("thread " + finalI + " run");
                    try {
                        Thread.sleep(2000);
                        countDown.countDown();

                        System.out.println("thread " + finalI + " end");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
        System.out.println(countDown.getCount());
        countDown.await();

        long stop = System.currentTimeMillis();
        System.out.println("main over total time=" + (stop-start));
    }
}
