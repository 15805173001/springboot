package com.luo.thread;

/**
 * 描述
 *
 * @author: 罗红兵
 * date:    2019/8/5 11:49
 */
public class TestJoin {
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("running");
            }
        }) ;
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("running2");
            }
        }) ;


        t1.start();
        //等待线程1终止
        t1.join();

        t2.start();
        //等待线程2终止
        t2.join();

        System.out.println("main over");
    }
}
