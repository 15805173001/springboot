package com.luo.test;

import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 描述
 *
 * @author: 罗红兵
 * date:    2019/7/24 11:49
 */

@Controller
@RequestMapping("/test")
public class TestController {

     public class Taa{
    }

    @ResponseBody
    @GetMapping("test1")
    public JSONObject test1(){
        JSONObject object = new JSONObject();
        object.put("code", "0000");
        object.put("msg", "success");
        return object;
    }

    @GetMapping("index")
    public String index(){
        return "index";
    }

    @RequestMapping("/index1")
    public String index1(Model model, Map<String, Object> map){
        map.put("name","map传参");
        model.addAttribute("name1","Model传参");
        return "index";
    }
}
