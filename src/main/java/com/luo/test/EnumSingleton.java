package com.luo.test;

import com.alibaba.fastjson.JSON;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * 描述
 *
 * @author: 罗红兵
 * date:    2019/8/8 16:17
 */

public enum EnumSingleton {
    TEST1,
    TEST2;

    String name;
    Integer age;


//    public EnumSingleton getEnumSingleton(){
//        return EnumSingleton;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        EnumSingleton test1 = EnumSingleton.TEST1;
        EnumSingleton test2 = EnumSingleton.TEST2;
        System.out.println(test1);
        System.out.println(test2);
        System.out.println(test1 == test2);

        Singleton singleton = Singleton.getInstance();
        System.out.println(singleton);
    }
}
