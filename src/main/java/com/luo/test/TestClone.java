package com.luo.test;

/**
 * 描述
 *
 * @author: 罗红兵
 * date:    2019/8/8 17:33
 */
public class TestClone implements Cloneable{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        TestClone testClone1 = new TestClone();
        testClone1.setName("name1");
        TestClone testClone2 = (TestClone)testClone1.clone();
        System.out.println(testClone1 == testClone2);
        testClone2.setName("name2");
        System.out.println(testClone1.getName());
        TestController.Taa taa = new TestController().new Taa();
    }

}
