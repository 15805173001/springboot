package com.luo.test;

/**
 * 描述
 *
 * @author: 罗红兵
 * date:    2019/8/8 15:56
 */
public class Singleton {


    private static volatile Singleton singleton;
    private Singleton(){};
    public static Singleton getInstance(){
        if (singleton == null) {
            synchronized (Singleton.class){
                if (singleton == null) {
                    singleton = new Singleton();
                }
            }
        }
        return singleton;
    }

}
