package com.luo.limit;

/**
 * 漏桶算法限流
 *
 * @author: 罗红兵
 * date:    2019/8/6 14:28
 */
public class LeakyBucketDemo {

    // 时间刻度
    private static long time = System.currentTimeMillis();
    // 桶里现在剩余的水
    private static volatile int water = 0;
    // 桶的大小
    private static int size = 10;
    // 出水速率
    private static volatile int rate = 3;

    public static boolean limit () {
        // 计算出水数量
        long now = System.currentTimeMillis();
        int out = (int)(now - time)/1000 * rate;
        // 剩余水量
        water = Math.max(water, water - out);
        System.out.println("water:" + (water - out) + "  rate:" + rate);
        time = now;
        if (water < size) {
            ++water;
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        for (int i=0;i<50;i++) {
            int finalI = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (limit()) {
                        System.out.println("业务处理...");
                        if (finalI == 5) {
                            System.out.println("修改 rate");
                            rate = 100;
                        }
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        --water;
                    } else {
                        System.out.println("限流...");
                    }
                }
            }).start();
        }
        Thread.sleep(3000);
        if (limit()) {
            System.out.println("3s后 业务处理...");
        } else {
            System.out.println("3s后 限流...");
        }
    }
}
