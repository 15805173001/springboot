package com.luo.limit;

import com.google.common.util.concurrent.RateLimiter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 令牌通算法限流
 *
 * @author: 罗红兵
 * date:    2019/8/6 15:17
 */
public class RateLimiterDemo {
    public static void main(String[] args) throws InterruptedException {
        double rate = 1;
        RateLimiter rateLimiter = RateLimiter.create(rate);
        long start = System.currentTimeMillis();
        for(int i=1;i<=8;i++){
            Thread.sleep(1000);
            Double acquire = null;
            if (i == 1) {
                acquire = rateLimiter.acquire(10);
            } else if (i == 2) {
                rateLimiter.setRate(100);
                Thread.sleep(2000);
//                System.out.println(rateLimiter.getRate());
//                rateLimiter = RateLimiter.create(2);
                acquire = rateLimiter.acquire(5);
            } else if (i == 3) {
                acquire = rateLimiter.acquire(20);
            } else if (i == 4) {
                acquire = rateLimiter.acquire(50);
            } else {
                acquire = rateLimiter.acquire(20);
            }
//            rateLimiter.acquire();
            System.out.println(" index:" + i + "  acquire:" + Math.round(acquire));
        }
        long end = System.currentTimeMillis();
        System.out.println("用时:" + (end - start));
    }
}
