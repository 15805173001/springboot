package com.luo.rabbit;

import com.rabbitmq.client.*;

/**
 * 描述
 *
 * @author: 罗红兵
 * date:    2019/7/24 14:59
 */
public class Consumer1 {

    private final static String QUEUE_01 = "queue_01";

    public static void main(String[] argv) throws Exception {
        // 获取到连接以及mq通道
        Connection connection = ConnectionUtil.getConnection();
        // 从连接中创建通道
        Channel channel = connection.createChannel();

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println("consumerTag " + consumerTag + " [consumer2] Received '" + message
                    + "'  DeliveryTag:" + delivery.getEnvelope().getDeliveryTag()
                    + "   thread " + Thread.currentThread().getName());

           /* try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
//            System.out.println("message:" + message);
//            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
//            channel.basicNack(delivery.getEnvelope().getDeliveryTag(), true, true);
//            channel.basicReject(delivery.getEnvelope().getDeliveryTag(), false);

        };
//        channel.basicQos(1);
        channel.basicConsume(QUEUE_01, true, deliverCallback, consumerTag -> {});

    }
}
