package com.luo.rabbit;

import com.rabbitmq.client.*;
import com.sun.org.apache.bcel.internal.generic.NEW;

import java.util.HashMap;
import java.util.Map;

/**
 * 描述
 *
 * @author: 罗红兵
 * date:    2019/7/24 11:30
 */
public class Producer {

    private final static String QUEUE_01 = "queue_01";

    private final static String QUEUE_02 = "queue_02";

    private final static String EXCHANGE_DIRECT_01 = "exchange_direct_01";

    private final static String EXCHANGE_FANOUT_01 = "exchange_fanout_01";

    private final static String EXCHANGE_TOPIC_01 = "exchange_topic_01";

    public static void main(String[] args) throws Exception {
        // 获取到连接以及mq通道
        Connection connection = ConnectionUtil.getConnection();
        // 创建通道
        Channel channel = connection.createChannel();
        // 声明交换机
        channel.exchangeDeclare(EXCHANGE_TOPIC_01, "topic", true);
        // 声明队列
        Map<String,Object> map = new HashMap<>();
//        map.put("x-max-length", 6);
//        map.put("x-overflow","reject-publish" );
        channel.queueDeclare(QUEUE_01, true, false, false, map);
//        channel.queueDeclare(QUEUE_02, true, false, false, map);

        channel.queueBind(QUEUE_01, EXCHANGE_TOPIC_01, "a.#");
//        channel.queueBind(QUEUE_02, EXCHANGE_TOPIC_01, "a.*");

        // 消息内容
        String message = "Hello World!";
        // 发送消息
        for(int i=0;i<10;i++) {
            String newmessage = message + i;
            /*for (int j = 0;j<1000;j++){
                newmessage += message;
            }*/

            channel.basicPublish(EXCHANGE_TOPIC_01, "a.112", MessageProperties.PERSISTENT_TEXT_PLAIN, newmessage.getBytes());
//            channel.basicPublish(EXCHANGE_TOPIC_01, "a.b",null, newmessage.getBytes());
            System.out.println(" [x] Sent '" + newmessage + "'");
        }

//        Thread.sleep(1000);
//        GetResponse getResponse = channel.basicGet(QUEUE_01, true);
//        System.out.println("msg count : " + getResponse.getMessageCount());
//        if (getResponse !=null && getResponse.getMessageCount() > 3) {
//            continue;
//        }

        //关闭通道和连接
        channel.close();
        connection.close();
    }
}
