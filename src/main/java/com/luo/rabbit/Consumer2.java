package com.luo.rabbit;

import com.rabbitmq.client.*;

/**
 * 描述
 *
 * @author: 罗红兵
 * date:    2019/7/24 14:59
 */
public class Consumer2 {

    private final static String QUEUE_01 = "queue_01";

    private final static String QUEUE_02 = "queue_02";

    public static void main(String[] argv) throws Exception {
        // 获取到连接以及mq通道
        Connection connection = ConnectionUtil.getConnection();
        // 从连接中创建通道
        Channel channel = connection.createChannel();

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println("consumerTag " + consumerTag + " [consumer2] Received '" + message
                    + "'  DeliveryTag:" + delivery.getEnvelope().getDeliveryTag()
                    + "   thread " + Thread.currentThread().getName());
//            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
//            channel.basicNack(delivery.getEnvelope().getDeliveryTag(), true, true);
//            channel.basicReject(delivery.getEnvelope().getDeliveryTag(), false);

        };
//        channel.basicQos(1);
        channel.basicConsume(QUEUE_02, true, deliverCallback, consumerTag -> {});
//        channel.basicConsume(QUEUE_01, true, deliverCallback, consumerTag -> {});
//        channel.basicConsume(QUEUE_02, true, deliverCallback, consumerTag -> {});
//

       /* // 获取消息
        while (true) {
            Consumer.Delivery delivery = consumer;
            String message = new String(delivery.getBody());
            System.out.println(" [x] Received '" + message + "'");
        }*/
    }
}
