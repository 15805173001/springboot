package com.luo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 描述
 *
 * @author: 罗红兵
 * date:    2019/7/24 11:42
 */
@SpringBootApplication(scanBasePackages = "com.luo")
public class LuoApplication {
    public static void main(String[] args) {
        SpringApplication.run(LuoApplication.class);
    }
}
